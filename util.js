

module.exports = {

    returnRandomUpTo: function (end) {
        return Math.floor(Math.random() * (end + 1));
    },

    returnRandomGoodbye: function () {
        let aResponses = ["Okay, see ya!", "Thankyou come again", "hasta la vista", "adios amigo"],
            iRand = this.returnRandomUpTo(aResponses.length);
        return aResponses[iRand];
    },

    greetingEasterEgg: function (session) {
        const userId = session.message.user.id;
        switch (userId) {
            case "neikumar":
                session.send("Greetings Father!");
                break;
            case "seacampbell":
                session.send("My creator, you have returned for me!");
                break;
            case "default-user":
                session.send("Don't forget to put me back together after testing!");
                break;
            default:
                break;
        }
    },

    returnUsernameAndProject(aUsers) {
        let aList = [];
        aUsers.forEach(user => {
            aList.push(user.username + " - " + user.projectName);
        });
        return aList;
    },

    returnUserDetails(oUser) {
        var sReturn = "";
        if (oUser.benched) {
            sReturn += "* **On the Bench:** ";
        } else {
            sReturn += "* **Project:** " + oUser.projectName;
        }
        sReturn += "\n* **Skills:** "

        sReturn += oUser.skills.map(e => e.skillName).join(",");

        return sReturn;
    },

    getUncommon(sentence, common) {
        var wordArr = sentence.match(/\w+/g),
            commonObj = {},
            uncommonArr = [],
            word, i;
    
        common = common.split(',');
        for ( i = 0; i < common.length; i++ ) {
            commonObj[ common[i].trim() ] = true;
        }
    
        for ( i = 0; i < wordArr.length; i++ ) {
            word = wordArr[i].trim().toLowerCase();
            if ( !commonObj[word] ) {
                uncommonArr.push(word);
            }
        }
    
        return uncommonArr;
    }

}